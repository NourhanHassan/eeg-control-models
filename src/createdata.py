import numpy as np
import os
import matplotlib.pyplot as plt
import time
from tensorflow.keras.models import model_from_json
#%%
ACTIONS = ["left","right","none"]
Starting_Training_Dir ="E:\Courses/2-Gitlab\Graduation_project\EEG-first-trial\model_data\data"
DATASEP = 5
img_dist_dir = os.path.join(Starting_Training_Dir, "data_imgs")
if not os.path.exists(img_dist_dir):
    os.mkdir(img_dist_dir)




#%%
def plot_file(file):
    plt.figure(figsize=(28,28))
    for channel in file[0:250]:
        plt.plot(channel)
#%%
def create_data_imgs(starting_dir = Starting_Training_Dir):
    training_data = {}
    for action in ACTIONS:
        action_dir = f"{img_dist_dir}/{action}"
        if not os.path.exists(action_dir):
            os.mkdir(action_dir)
        if action not in training_data:
            training_data[action] = []
        data_dir = os.path.join(starting_dir, action)

        for item in os.listdir(data_dir):
            data = np.load(os.path.join(data_dir, item))
            plot_file(data)
            plt.savefig(f"{action_dir}/{int(time.time())}.png", bbox_inches='tight')



#%%
create_data_imgs()



#%%
def Create_data(starting_dir = Starting_Training_Dir):
    training_data = {}
    for action in ACTIONS:
        if action not in training_data:
            training_data[action] = []

        data_dir = os.path.join(starting_dir,action)
        for item in os.listdir(data_dir):
            #print(action, item)
            data = np.load(os.path.join(data_dir, item))
            for idx, item in enumerate(data):
            	# adding this for cushion between insample data.
            	if idx % DATASEP == 0:
                	training_data[action].append(item)

    lengths = [len(training_data[action]) for action in ACTIONS]
    print(lengths)

    for action in ACTIONS:
        np.random.shuffle(training_data[action])
        training_data[action] = training_data[action][:min(lengths)]

    lengths = [len(training_data[action]) for action in ACTIONS]
    print(lengths)
    # creating X, y
    combined_data = []
    for action in ACTIONS:
        for data in training_data[action]:

            if action == "left":
                combined_data.append([data, [1, 0, 0]])

            elif action == "right":
                #np.append(combined_data, np.array([data, [1, 0]]))
                combined_data.append([data, [0, 0, 1]])

            elif action == "none":
                combined_data.append([data, [0, 1, 0]])

    np.random.shuffle(combined_data)
    print("length:",len(combined_data))
    return combined_data

def get_val_data(valdir,action,session_file,CLIP_VAL):
    action_dir = os.path.join(valdir, action)
    filepath = os.path.join(action_dir,session_file)
    data = np.clip(np.load(filepath), -CLIP_VAL, CLIP_VAL) / CLIP_VAL
    return data
# class ControlModel(object):
#
#
#     def __init__(self, model_json_file, model_weights_file):
#         # load model from JSON file
#         with open(model_json_file, "r") as json_file:
#             loaded_model_json = json_file.read()
#             self.loaded_model = model_from_json(loaded_model_json)
#
#         # load weights into the new model
#         self.loaded_model.load_weights(model_weights_file)
#         self.loaded_model.make_predict_function()
#
#     def predict_command(self, file):
#         self.preds = self.loaded_model.predict(file)
#         return ControlModel.ACTIONS[np.argmax(self.preds)]


