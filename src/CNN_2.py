from tensorflow.keras.layers import Dense, Input, Dropout,Flatten, Conv2D
from tensorflow.keras.layers import BatchNormalization, MaxPooling2D, AveragePooling2D,LeakyReLU
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau,EarlyStopping
from tensorflow.keras.utils import plot_model
from livelossplot.inputs.tf_keras import PlotLossesCallback
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import matplotlib.pyplot as plt
import os
import numpy as np
from IPython.display import SVG, Image
#%%
Starting_Training_Dir ="E:\Courses/2-Gitlab\Graduation_project\EEG-first-trial\model_data\data\data_imgs"
Starting_Validation_Dir ="E:\Courses/2-Gitlab\Graduation_project\EEG-first-trial\model_data/validation_data\data_imgs"

#%%
plot_example_images(plt).show()
#%%
img_size = 48
batch_size = 64
#datagen_train = smart_resize()
datagen_train = ImageDataGenerator(horizontal_flip=False,fill_mode="nearest",
                                   rescale=1./255)

train_generator = datagen_train.flow_from_directory(Starting_Training_Dir,
                                                    target_size=(img_size,img_size),
                                                    color_mode="grayscale",
                                                    batch_size=batch_size,
                                                    class_mode='categorical',
                                                    interpolation="bilinear",
                                                    shuffle=True)

datagen_validation = ImageDataGenerator(horizontal_flip=False,
                                        fill_mode="nearest",
                                        rescale=1./ 255)
validation_generator = datagen_validation.flow_from_directory(Starting_Validation_Dir,
                                                    target_size=(img_size,img_size),
                                                    color_mode="grayscale",
                                                    batch_size=batch_size,
                                                    class_mode='categorical',
                                                    interpolation="bilinear",
                                                    shuffle=False)

#%%
#construct CNN structure
model = Sequential()

#1st convolution layer
model.add(Conv2D(64, (3,3), activation='relu', input_shape=(48,48,1),data_format='channels_last'))
model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2)))
model.add(BatchNormalization())

#2nd convolution layer
model.add(Conv2D(265, (3, 3), activation='relu'))
model.add(Conv2D(265, (3, 3), activation='relu'))
model.add(AveragePooling2D(pool_size=(3,3), strides=(2, 2)))
model.add(BatchNormalization())

#3rd convolution layer
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(AveragePooling2D(pool_size=(3,3), strides=(2, 2)))
model.add(BatchNormalization())

model.add(Flatten())

#fully connected neural networks
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Dense(7, activation='softmax'))

opt = Adam(lr=0.0005)#
model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

#%%
epochs = 15
steps_per_epoch = train_generator.n//train_generator.batch_size
validation_steps = validation_generator.n//validation_generator.batch_size

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=2, min_lr=0.0001, mode='auto') #0.00001
checkpoint = ModelCheckpoint("model_weights_15_LR.h5", monitor='val_accuracy',
                             save_weights_only=True, mode='max', verbose=1)
#earlystop_callback = EarlyStopping(monitor='val_accuracy', min_delta=0.0001,patience=1)
callbacks = [PlotLossesCallback(), checkpoint, reduce_lr]
history = model.fit(
    x=train_generator,
    steps_per_epoch=steps_per_epoch,
    epochs=epochs,
    validation_data = validation_generator,
    validation_steps = validation_steps,
    callbacks=callbacks
)
#%%

