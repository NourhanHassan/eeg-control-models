#%%
import os
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tensorflow.keras.models import model_from_json

#%%
MODEL_NAME = "new_models/5.model"
weights = "E:\Courses/2-Gitlab\Graduation_project/EEG-first-trial/src/model_weights_10_v6.h5"
CLIP = True  # if your model was trained with np.clip to clip  values
CLIP_VAL = 3  # if above, what was the value +/-
PRED_BATCH = 64

VALDIR = 'E:/validation_data'
#%%
model = tf.keras.models.load_model(MODEL_NAME)


#%%
class ControlModel(object):

    #ACTIONS = ["left","right","none"]

    def __init__(self, model_json_file, model_weights_file):
        # load model from JSON file
        with open(model_json_file, "r") as json_file:
            loaded_model_json = json_file.read()
            self.loaded_model = model_from_json(loaded_model_json)

        # load weights into the new model
        self.loaded_model.load_weights(model_weights_file)
        #self.loaded_model.make_predict_function()

    # def predict_command(self, file):
    #     self.preds = self.loaded_model.predict(file)
        #return ControlModel.ACTIONS[np.argmax(self.preds)]
#%%

model = ControlModel(MODEL_NAME,weights)

#%%
def get_val_data(valdir, action, batch_size):

    argmax_dict = {0: 0, 1: 0, 2: 0}
    raw_pred_dict = {0: 0, 1: 0, 2: 0}

    action_dir = os.path.join(valdir, action)
    for session_file in os.listdir(action_dir):
        filepath = os.path.join(action_dir,session_file)
        if CLIP:
            data = np.clip(np.load(filepath), -CLIP_VAL, CLIP_VAL) / CLIP_VAL
        else:
            data = np.load(filepath)

        #preds = model.loaded_model.predict([data.reshape(-1, 16, 60)])
        preds = model.predict([data.reshape(-1, 16, 60)])

        for pred in preds:
            argmax = np.argmax(pred)
            argmax_dict[argmax] += 1
            for idx,value in enumerate(pred):
                raw_pred_dict[idx] += value

    argmax_pct_dict = {}

    for i in argmax_dict:
        total = 0
        correct = argmax_dict[i]
        for ii in argmax_dict:
            total += argmax_dict[ii]

        argmax_pct_dict[i] = round(correct/total, 3)

    return argmax_dict, raw_pred_dict, argmax_pct_dict
#%%
def make_conf_mat(left, none, right):

    action_dict = {"left": left, "none": none, "right": right}
    action_conf_mat = pd.DataFrame(action_dict)
    actions = [i for i in action_dict]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.matshow(action_conf_mat, cmap=plt.cm.RdYlGn)
    ax.set_xticklabels([""]+actions)
    ax.set_yticklabels([""]+actions)

    print("__________")
    print(action_dict)
    for idx, i in enumerate(action_dict):
        print('tf',i)
        for idx2, ii in enumerate(action_dict[i]):
            print(i, ii)
            print(action_dict[i][ii])
            ax.text(idx, idx2, f"{round(float(action_dict[i][ii]),2)}", va='center', ha='center')
    plt.title("Action Thought")
    plt.ylabel("Predicted Action")
    plt.show()
#%%
left_argmax_dict, left_raw_pred_dict, left_argmax_pct_dict = get_val_data(VALDIR, "left", PRED_BATCH)
none_argmax_dict, none_raw_pred_dict, none_argmax_pct_dict = get_val_data(VALDIR, "none", PRED_BATCH)
right_argmax_dict, right_raw_pred_dict, right_argmax_pct_dict = get_val_data(VALDIR, "right", PRED_BATCH)

make_conf_mat(left_argmax_pct_dict, none_argmax_pct_dict, right_argmax_pct_dict)


#%%
def get_val_data(valdir,action,session_file,CLIP_VAL):
    action_dir = os.path.join(valdir, action)
    filepath = os.path.join(action_dir,session_file)
    data = np.clip(np.load(filepath), -CLIP_VAL, CLIP_VAL) / CLIP_VAL
    return data
#%%
class ControlModel(object):
    ACTIONS = ["left", "right", "none"]
    def __init__(self, model_json_file, model_weights_file):
        # load model from JSON file
        with open(model_json_file, "r") as json_file:
            loaded_model_json = json_file.read()
            self.loaded_model = model_from_json(loaded_model_json)

        # load weights into the new model
        self.loaded_model.load_weights(model_weights_file)
        #self.loaded_model.make_predict_function()

    def predict_command(self, file):
        self.preds = self.loaded_model.predict(file)
        return ControlModel.ACTIONS[np.argmax(self.preds)]
#%%
model = ControlModel(MODEL_NAME,weights)
result = model.predict_command(get_val_data(VALDIR,"left",'1573486141.npy',3))
#
#%%
print(result)
