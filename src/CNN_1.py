#%%
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Dropout,Activation,Flatten,Reshape,\
    GaussianNoise,Conv1D,MaxPooling1D,BatchNormalization
from tensorflow.keras.layers import Reshape

#%%
Starting_Validation_Dir = 'E:\Courses/2-Gitlab\Graduation_project\EEG-first-trial\model_data/validation_data'
reshape = (-1, 16, 60)

#%%
from createdata import Create_data

#%%
print("creating training data")

traindata = Create_data()

train_X = []
train_y = []
for X, y in traindata:
    train_X.append(X)
    train_y.append(y)

#%%

print("creating testing data")
testdata = Create_data(starting_dir=Starting_Validation_Dir)
test_X = []
test_y = []
for X, y in testdata:
    test_X.append(X)
    test_y.append(y)

#%%
import matplotlib.pyplot as plt
import numpy as np

#%%
np.shape(train_X)
#%%

"""
train_X = np.clip(np.array(train_X).reshape(reshape) - np.mean(train_X), -10, 10) / 10
test_X = np.clip(np.array(test_X).reshape(reshape) - np.mean(test_X), -10, 10) / 10

train_y = np.array(train_y)
test_y = np.array(test_y)
"""
#%%
np.shape(train_X)

#%%
train_X = np.clip(np.array(train_X).reshape(reshape), -3, 3)
test_X = np.clip(np.array(test_X).reshape(reshape), -3, 3)


train_y = np.array(train_y)
test_y = np.array(test_y)
#%%
#from sklearn.decomposition import PCA
#%%

pca_std = np.std(train_X)

#%%
"""
pca = PCA(n_components=3)
pca.fit(train_X)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('Number of components')
plt.ylabel('Cumulative explained variance')
"""
#%%
model = Sequential()
initializer = tf.keras.initializers.RandomUniform(-1, 1)
model.add(Conv1D(1024, (5), padding='SAME',
                 input_shape=train_X.shape[1:],
                 kernel_regularizer = tf.keras.regularizers.l1_l2(l1=0.005,l2=0.001),
                 bias_regularizer= tf.keras.regularizers.l2(0.001),
                 kernel_initializer=initializer))
#model.add(GaussianNoise(pca_std))
model.add(Activation('relu'))
model.add(BatchNormalization())

model.add(Conv1D(32, (5), padding='SAME'))
model.add(GaussianNoise(pca_std))
model.add(Activation('relu'))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(Conv1D(16, (3), padding='SAME'))
model.add(GaussianNoise(pca_std))
model.add(Activation('relu'))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(Conv1D(16, (3), padding='same'))
model.add(GaussianNoise(pca_std))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Conv1D(3, (16)))
model.add(Reshape((3,)))
model.add(Activation('softmax'))

#%%

model.summary()

#%%
from tensorflow.keras.optimizers import Nadam
from tensorflow.keras.losses import CategoricalCrossentropy
#%%
opt = Nadam(lr=0.0005)
model.compile(loss= CategoricalCrossentropy(),
              optimizer= opt,
              metrics=['accuracy'])


#%%
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau,EarlyStopping
from livelossplot.inputs.tf_keras import PlotLossesCallback
#%%

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=2, min_lr=0.00001, mode='auto') #0.00001
checkpoint = ModelCheckpoint("model_weights_10_v10.h5", monitor='val_accuracy',
                             save_weights_only=True, mode='max', verbose=1)
callbacks = [PlotLossesCallback(), checkpoint, reduce_lr]
#%%
epochs = 15
batch_size = 64

#%%
history = model.fit(train_X,
          train_y,
          batch_size=batch_size,
          epochs= epochs,
          validation_split = 0.2,
          callbacks=callbacks)

#%%
import pandas as pd
df = pd.DataFrame(history.history)
df.tail()

#%%
loss, accuracy = model.evaluate(test_X,test_y,verbose = 2)

#%%
"""
model_json = model.to_json()
with open("model_v6_10.json", "w") as json_file:
    json_file.write(model_json)
"""

#%%
"""
for epoch in range(epochs):
    model.fit(train_X, train_y, batch_size=batch_size, epochs=1, validation_data=(test_X, test_y))
    score = model.evaluate(test_X, test_y, batch_size=batch_size)
    MODEL_NAME = "{}.model".format(epoch)
    model.save(MODEL_NAME)
print("saved:")
print(MODEL_NAME)

"""
#%%
"""
config = tf.compat.v1.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.15
session = tf.compat.v1.Session(config=config)

"""

#%%
# Run this cell to get model predictions on randomly selected test images

num_test_files = test_X.shape[0]

random_inx = np.random.choice(num_test_files, 4)  #4
random_test_files = test_X[random_inx, ...]
random_test_labels = test_y[random_inx, ...]
class_names = ['left','none','right']

predictions = model.predict(random_test_files)

fig, axes = plt.subplots(4, 2, figsize=(16, 12))
fig.subplots_adjust(hspace=0.4, wspace=-0.2)

for i, (prediction, image, label) in enumerate(zip(predictions, random_test_files, random_test_labels)):
    axes[i, 0].imshow(np.squeeze(image))
    axes[i, 0].get_xaxis().set_visible(False)
    axes[i, 0].get_yaxis().set_visible(False)
    axes[i, 0].text(10., -1.5, f'direction {class_names[np.argmax(label)]}')
    axes[i, 1].bar(np.arange(len(prediction)), prediction)
    axes[i, 1].set_xticks(np.arange(len(prediction)))
    axes[i, 1].set_title(f"Model prediction:  {class_names[np.argmax(prediction)]}")

plt.show()