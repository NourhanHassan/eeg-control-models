from datetime import datetime
import io
import itertools
import sklearn
from livelossplot import tensorboard
from tensorflow.keras.layers import Reshape,Activation,Dense,\
    BatchNormalization,Dropout,Flatten,Conv1D
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau,EarlyStopping
from tensorflow.keras.utils import plot_model
from livelossplot.inputs.tf_keras import PlotLossesCallback
import tensorflow as tf
from sklearn.model_selection import KFold
#%%
Starting_Validation_Dir = 'E:\Courses/2-Gitlab\Graduation_project\EEG-first-trial\model_data/validation_data'
reshape = (-1, 16, 60)

#%%
from createdata import Create_data

#%%
print("creating training data")

traindata = Create_data()

train_X = []
train_y = []
for X, y in traindata:
    train_X.append(X)
    train_y.append(y)

#%%

print("creating testing data")
testdata = Create_data(starting_dir=Starting_Validation_Dir)
test_X = []
test_y = []
for X, y in testdata:
    test_X.append(X)
    test_y.append(y)




#%%
import matplotlib as plt
import numpy as np


#%%
train_X = np.clip(np.array(train_X).reshape(reshape), -3, 3)
test_X = np.clip(np.array(test_X).reshape(reshape), -3, 3)


train_y = np.array(train_y)
test_y = np.array(test_y)


#%%
model = Sequential()
initializer = tf.keras.initializers.RandomUniform(-1, 1)
model.add(Conv1D(64, (5), padding='SAME', input_shape=train_X.shape[1:],
                 kernel_initializer=initializer,kernel_regularizer= tf.keras.regularizers.l1_l2(l1=0.005,l2=0.001)))
model.add(Activation('relu'))
model.add(BatchNormalization())

model.add(Conv1D(128, (5), padding='SAME'))
model.add(Activation('relu'))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(Conv1D(256, (5), padding='SAME'))
model.add(Activation('relu'))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(Conv1D(512, (5), padding='same'))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Conv1D(3, (16)))
model.add(Reshape((3,)))
model.add(Activation('softmax'))

#%%
from tensorflow.keras.optimizers import Nadam
from tensorflow.keras.losses import CategoricalCrossentropy



opt = Nadam(lr=0.0005)
model.compile(loss= CategoricalCrossentropy(),
              optimizer= opt,
              metrics=['accuracy'])


#%%
model.summary()

#%%

epochs = 7
batch_size = 64

#%%
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau,EarlyStopping
from livelossplot.inputs.tf_keras import PlotLossesCallback


#%%
def plot_confusion_matrix(cm, class_names):

    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names)

    # Normalize the confusion matrix.
    cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.

    for i, j in np.itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        color = "white" if cm[i, j] > threshold else "black"
        plt.text(j, i, cm[i, j], horizontalalignment="center", color=color)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure



#%%

def plot_to_image(figure):


    buf = tf.io.BytesIO()

    # Use plt.savefig to save the plot to a PNG in memory.
    plt.savefig(buf, format='png')

    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)

    # Use tf.image.decode_png to convert the PNG buffer
    # to a TF image. Make sure you use 4 channels.
    image = tf.image.decode_png(buf.getvalue(), channels=4)

    # Use tf.expand_dims to add the batch dimension
    image = tf.expand_dims(image, 0)

    return image
#%%
class_names = ['left','none','right']

def log_confusion_matrix(epoch, logs):
    # Use the model to predict the values from the test_images.
    test_pred_raw = model.predict(test_X)

    test_pred = np.argmax(test_pred_raw, axis=1)

    # Calculate the confusion matrix using sklearn.metrics
    cm = sklearn.metrics.confusion_matrix(test_y, test_pred)

    figure = plot_confusion_matrix(cm, class_names=class_names)
    cm_image = plot_to_image(figure)

    # Log the confusion matrix as an image summary.
    with file_writer_cm.as_default():
        tf.summary.image("Confusion Matrix", cm_image, step=epoch)
#%%

logdir = "logs/image/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir = logdir, histogram_freq = 1,profile_batch = 100000000)
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')
cm_callback = tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix)


#%%
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=2, min_lr=0.00001, mode='auto') #0.00001
checkpoint = ModelCheckpoint("model_weights_15_v8.h5", monitor='val_accuracy',
                             save_weights_only=True, mode='max', verbose=1)
callbacks = [PlotLossesCallback(), checkpoint,tensorboard_callback, cm_callback]

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=2, min_lr=0.00001, mode='auto') #0.00001
checkpoint = ModelCheckpoint("model_weights_15_v8.h5", monitor='val_accuracy',
                             save_weights_only=True, mode='max', verbose=1)
callbacks = [PlotLossesCallback(), checkpoint, reduce_lr,tensorboard, cm_callback]




#%%
# Define total folds
num_folds = 10


# Define per-fold score containers

loss_per_fold = []
acc_per_fold  = []

# Define the K-fold Cross Validator
kfold = KFold(n_splits=num_folds, shuffle=True)

# K-fold Cross Validation model evaluation
fold_no = 1

for train, test in kfold.split(train_X,train_y):
    x_train = train_X[train]
    y_train = train_y[train]
    x_test  = train_X[test]
    y_test  = train_y[test]
    opt = Nadam(lr=0.0005)
    model.compile(loss=CategoricalCrossentropy(),
                  optimizer=opt,
                  metrics=['accuracy'])
    epochs = 4
    batch_size = 32
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                  patience=2, min_lr=0.00001, mode='auto')  # 0.00001
    checkpoint = ModelCheckpoint("model_weights_15_v8.h5", monitor='val_accuracy',
                                 save_weights_only=True, mode='max', verbose=1)
    callbacks = [PlotLossesCallback(), checkpoint, reduce_lr]
    for epoch in range(epochs):
        history = model.fit(x_train,
                            y_train,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_data=(x_test, y_test),
                            callbacks=callbacks)
        score = model.evaluate(test_X, test_y, batch_size=batch_size)
        MODEL_NAME = "{}_{}.model".format(num_folds,epoch)
        model.save(MODEL_NAME)


#%%
loss, accuracy = model.evaluate(test_X,test_y)

