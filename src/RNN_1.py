
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Dropout,Activation,SimpleRNN,LSTM,GRU,Conv1D,\
    GaussianNoise,BatchNormalization
#%%

import os
import datetime


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#import seaborn as sns



#%%
Starting_Validation_Dir = 'E:/validation_data'
reshape = (-1, 16, 60)

#%%
from createdata import Create_data

#%%
print("creating training data")

traindata = Create_data()

train_X = []
train_y = []
for X, y in traindata:
    train_X.append(X)
    train_y.append(y)


#%%

print("creating testing data")
testdata = Create_data(starting_dir=Starting_Validation_Dir)
test_X = []
test_y = []
for X, y in testdata:
    test_X.append(X)
    test_y.append(y)

print(len(train_X))
print(len(test_X))



#%%
train_X = np.clip(np.array(train_X).reshape(reshape), -3, 3)
test_X = np.clip(np.array(test_X).reshape(reshape), -3, 3)


train_y = np.array(train_y)
test_y = np.array(test_y)


#%%
pca_std = np.std(train_X)

#%%
from tensorflow.keras.optimizers import RMSprop,Nadam

#%%
# create and fit the SimpleRNN model
n_classes = 3
model = Sequential()
#initializer = tf.keras.initializers.RandomUniform(-1, 1)


model.add(GRU(units=16, activation='relu',input_shape=train_X.shape[1:]))

#model.add(Dense(filters= 128,activation='relu'))
#model.add(Dense(64,activation='relu'))
model.add(Dense(n_classes))
model.add(Activation('softmax'))


#%%
model.compile(loss='categorical_crossentropy',
 optimizer=Nadam(lr=0.005),
 metrics=['accuracy'])
#%%
model.summary()
#%%
batch_size=128
epochs=10

#%%
model.fit(train_X, train_y,
 batch_size=batch_size, epochs=epochs)
score = model.evaluate(test_X, test_y)
print('\nTest loss:', score[0])
print('Test accuracy:', score[1])

#%%
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau,EarlyStopping
from livelossplot.inputs.tf_keras import PlotLossesCallback

#%%
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=2, min_lr=0.00001, mode='auto') #0.00001
checkpoint = ModelCheckpoint("model_weights_10_v2.h5", monitor='val_accuracy',
                             save_weights_only=True, mode='max', verbose=1)
callbacks_1 = [PlotLossesCallback(), checkpoint, reduce_lr]
callbacks_2 = [PlotLossesCallback(), reduce_lr]
#%%
history = model.fit(train_X,
          train_y,
          validation_split=0.2,
          batch_size=batch_size,
          epochs= epochs,
          callbacks=callbacks_1)
#%%
df = pd.DataFrame(history.history)
df.tail()
#%%
for epoch in range(epochs):
    model.fit(train_X, train_y, batch_size=batch_size, epochs=1, validation_data=(test_X, test_y),callbacks=callbacks_2)
    score = model.evaluate(test_X, test_y, batch_size=batch_size)
    MODEL_NAME = "{}.model".format(epoch)
    model.save(MODEL_NAME)
print("saved:")
print(MODEL_NAME)
print('\nTest loss:', score[0])
print('Test accuracy:', score[1])