# EEG_Control_Models_1

This repository contains our initial attempts to create control models using EEG signals.

## Dataset

[download data](https://hkinsley.com/static/downloads/bci/model_data_v2.7z)
#### FFT_signal
File structure (for both the data and validation_data directories):
- **data**
    - left
    - none
    - right
- **validation_data**
    - left
    - none
    - right 

The data available is 16-channel FFT 0-60Hz, sampled at a rate of about 25/second. \
Data is contained in directories labeled as `left`,`right`,`none`. \
These directories contain numpy arrays of this FFT data collected where someone was thinking of moving a square on the screen in this directions. 

<img src="https://gitlab.com/NourhanHassan/eeg_models/-/raw/923c6d04e60c96242149d5c00c1467c857cd1faf/Picture1.png" width="350" height="300">

#### FFT spectral maps

<img src="https://gitlab.com/NourhanHassan/eeg_models/-/raw/master/left.PNG" width="500" height="350">


## Brain-Computer Interface workspace
This is our first trial training a model with a dataset that has been extracted from the OpenBCI 16-channel headset.

This is merely an example of training several models using many ML and DL techniques to get the best results with this type of sequence data. 


best results with this type of sequence data. 


| CNN Architecture |Activation|epochs| Input formulation | Training Accuracy(%)|Testing Accuracy(%)|File of model_weights|
| ---------------- |----------|------|------------------ |-------------------- |------------------ |--------------------- |
| 3 Conv, 2 Maxpooling , 2 FC | ReLU (conv), Linear(FC1)  , Softmax(FC2) | 10 |   FFT     | 96     | 41 |model_weights_10_v1.h5| 
|3 Conv, 3 Maxpooling , 3 Batchnormalization , 2 Dropout , 2 FC| ReLU (conv), ReLU(FC1)  , Softmax(FC2)  |7 |   FFT    |96.9|45.9|model_weights_10_v2.h5|
| 5 Conv with 'SAME' padding  |  ReLU (conv1-4), Softmax(conv5) |10| FFT |98.4 | 45.8 |model_weights_10_v3.h5|
|  5 Conv with 'SAME' padding , 4 GaussianNoise  |  ReLU (conv1-4), Softmax(conv5)  |  10   |Augmented FFT|76.7 |42.8|model_weights_10_v4.h5|
|5 Conv with 'SAME' padding , 4 GaussianNoise | ReLU (conv1-4), Softmax(conv5) |     15    |Augmented FFT|80.9|42.8|model_weights_15_v4.h5|
| 5 Conv with 'SAME' padding, 4 Batchnormalization,1 Dropout, 2 GaussianNoise| ReLU (conv1-4), Softmax(conv5) |     10    |Augmented FFT|97.9|44.8|model_weights_10_v5.h5|
| 5 Conv with 'SAME' padding, 3 Batchnormalization,3 Dropout, 3 GaussianNoise | ReLU (conv1-4), Softmax(conv5) |     10    |Augmented FFT |74.6|45.5|model_weights_10_v6.h5|
| 5 Conv with 'SAME' padding, 4 Batchnormalization,4 Dropout | ReLU (conv1-4), Softmax(conv5) |     5    |Augmented FFT |98.5|98.3|model_weights_10_v10.h5|

**Best performance model results**
<img src= "https://gitlab.com/NourhanHassan/eeg_models/-/raw/master/OUT.png" width="500" height="250"> 

    - `TRAINING`  98%
    - `VALIDATION`  98.3%
    - `TESTING`  91%

| RNN Architecture |Activation|epochs| Input formulation | Training Accuracy(%)|Testing Accuracy(%)|
| ---------------- |----------|------|------------------ |-------------------- |------------------ |
| 1 SimpleRNN,  3 FC | ReLU   , Softmax(FC3) | 10 |   FFT     | 52    | 40 | 
|1 Conv ,1 GRU, 4 FC | ReLU (conv), ReLU(FC1) , Softmax(FC4)  |10|   FFT    |65|42|
|2 Conv ,1 GRU, 3 FC   2 Batchnormalization , 2 Dropout |  ReLU , Softmax(FC3) |10| FFT |70.6 | 43.5 |
|3 Conv ,1 LSTM, 3 FC   3 Batchnormalization , 3 Dropout |  ReLU , Softmax(FC3)  |  10   |Augmented FFT|73 |42.8|

## Files
**`src`** folder contains all source codes`CNN_1.py` which is the  CNN training model for FFT data without k-means cross validation ,`CNN_4.py` which is the last CNN training model for FFT data with k-means cross validation, `CNN_2.py` CNN training model for FFT-mapes images data ,`RNN_1.py` which is the  RNN training model for FFT data without k-means cross validation,`createdata.py` contains functions and classes used to divide the dataset into training and testing and `testing.py` to test each created model using validation data. **`CNN_saved_models`** folder which contains all the saved CNN models and weights.

**`results`** contains all the documented results for all the models.

<img src= "https://gitlab.com/NourhanHassan/eeg_models/-/raw/master/result1.PNG" width="500" height="400"> <img src= "https://gitlab.com/NourhanHassan/eeg_models/-/raw/master/result2.PNG" width="500" height="400">

## CONCLUSION


For colab notebooks: [GoogleDrive](https://drive.google.com/drive/folders/1KDV7Khw3Ep2KsI_O10xfAw3PWncgjnQy?usp=sharing)
